<?php

namespace App\Controller;

use App\Tests\ApiTestCase;

class UserControllerTest extends ApiTestCase
{

    public function testGetAllEmpty()
    {
        $client = static::createClient();
        $client->request('GET', '/users/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals([], json_decode($client->getResponse()->getContent(), true));
    }

    public function testGetAllNotEmpty()
    {
        $user = $this->createUser();
        $client = static::createClient();
        $client->request('GET', '/users/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(1, $content);
        $content = $content[0];
        $this->assertEquals($user->getPseudo(), $content["pseudo"]);
        $this->assertEquals(null, $content["password"]);
    }

    public function testGetById()
    {
        $user = $this->createUser();
        $client = static::createClient();
        $client->request('GET', '/users/' . $user->getId());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals($user->getPseudo(), $content["pseudo"]);
        $this->assertEquals(null, $content["password"]);
    }

    public function testGetByIdNotFound()
    {
        $client = static::createClient();
        $client->request('GET', '/users/1');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    public function testCanAdd()
    {
        $client = static::createClient();
        $client->request('POST', '/users/add', array("pseudo" => "mitsi", "password" => "dev"));
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $this->assertNotNull($this->userRepository->findOneBy(array("pseudo" => "mitsi")));
    }

    public function testCannotAddSamePseudo()
    {
        $client = static::createClient();
        $client->request('POST', '/users/add', array("pseudo" => "mitsi", "password" => "dev"));
        $client->request('POST', '/users/add', array("pseudo" => "mitsi", "password" => "dev"));
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testCannotAddWithMissingArgument()
    {
        $client = static::createClient();
        $client->request('POST', '/users/add', array("pseudo" => "mitsi"));
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $client->request('POST', '/users/add', array("password" => "mitsi"));
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testCannotEditIfCannotConnect()
    {
        $client = static::createClient();
        $user = $this->createUser();
        $client->request('PUT', '/users/edit', array("token" =>self::forgeToken($user->getPseudo(), $user->getPassword()."a"), "newPassword" => "a"));
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }

    public function testCanEdit()
    {
        $client = static::createClient();
        $user = $this->createUser();
        $client->request('PUT', '/users/edit', array("token" => self::forgeToken($user->getPseudo(), $user->getPassword()), "newPassword" => "a"));
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
    }

    public function testCannotEditWithMissingArgument()
    {
        $client = static::createClient();
        $user = $this->createUser();
        $client->request('PUT', '/users/edit', array("token" => self::forgeToken($user->getPseudo(), "")));
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $client->request('PUT', '/users/edit', array("token" => self::forgeToken($user->getPseudo(), $user->getPassword())));
        $client->request('PUT', '/users/edit', array("token" => self::forgeToken("", $user->getPassword()), "newPassword" => "a"));
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testCannotDeleteWithMissingArgument()
    {
        $client = static::createClient();
        $client->request('DELETE', '/users/delete', array("token" => self::forgeToken("mitsi", "")));
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testCanDelete()
    {
        $client = static::createClient();
        $user = $this->createUser();
        $client->request('DELETE', '/users/delete', array("token" => self::forgeToken($user->getPseudo(), $user->getPassword())));
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
    }

    public function testDeleteNotFound()
    {
        $client = static::createClient();
        $client->request('DELETE', '/users/delete', array("token" => self::forgeToken("a", "a")));
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }
}
