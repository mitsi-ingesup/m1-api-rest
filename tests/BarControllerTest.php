<?php
/**
 * Created by PhpStorm.
 * User: mitsi
 * Date: 14/01/2018
 * Time: 11:11
 */

namespace App\Controller;


use App\Tests\ApiTestCase;

class BarControllerTest extends ApiTestCase
{

    public function testGetAllEmpty()
    {
        $client = static::createClient();
        $client->request('GET', '/bars/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals([], json_decode($client->getResponse()->getContent(), true));
    }

    public function testGetAll()
    {
        $bar = $this->createBar();
        $client = static::createClient();
        $client->request('GET', '/bars/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(1, $content);
        $content = $content[0];

        $this->assertEquals($bar->getName(), $content["name"]);
        $this->assertEquals($bar->getAddress(), $content["address"]);
        $this->assertEquals($bar->getHappyHourStart(), $content["happyHourStart"]);
        $this->assertEquals($bar->getHappyHourEnd(), $content["happyHourEnd"]);
        $this->assertEquals($bar->getLatitude(), $content["latitude"]);
        $this->assertEquals($bar->getLongitude(), $content["longitude"]);
        $this->assertEquals([], $content["beers"]);
    }

    public function testGet()
    {
        $bar = $this->createBar();
        $beer = $this->createBeer();
        $this->associateBarWithBeer($bar, $beer);
        $client = static::createClient();
        $client->request('GET', '/bars/' . $bar->getId());

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals($bar->getName(), $content["name"]);
        $this->assertEquals($bar->getAddress(), $content["address"]);
        $this->assertEquals($bar->getHappyHourStart(), $content["happyHourStart"]);
        $this->assertEquals($bar->getHappyHourEnd(), $content["happyHourEnd"]);
        $this->assertEquals($bar->getLatitude(), $content["latitude"]);
        $this->assertEquals($bar->getLongitude(), $content["longitude"]);
        $beersApi = $content["beers"];
        $this->assertCount(1, $beersApi);
        $beerApi = $beersApi[0];
        $this->assertCount(2, $beerApi);
        $this->assertEquals($beer->getId(), $beerApi["id"]);
        $this->assertEquals($beer->getName(), $beerApi["name"]);
    }

    public function testAdd()
    {
        $user = $this->createUser();
        $client = static::createClient();
        $data = array(
            "token" => $this->forgeTokenFromUser($user),
            "name" => "mitsi",
            "address" => "dev",
            "latitude" => 48.866667,
            "longitude" => 2.333333,
            "happyHourStart" => 18,
            "happyHourEnd" => 19
        );
        $client->request('POST', '/bars/add',
            $data
        );
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $bar = $this->barRepository->findOneBy(array("name" => $data["name"]));

        $this->assertEquals($bar->getName(), $data["name"]);
        $this->assertEquals($bar->getAddress(), $data["address"]);
        $this->assertEquals($bar->getLatitude(), $data["latitude"]);
        $this->assertEquals($bar->getLongitude(), $data["longitude"]);
        $this->assertEquals($bar->getHappyHourStart(), $data["happyHourStart"]);
        $this->assertEquals($bar->getHappyHourEnd(), $data["happyHourEnd"]);
    }

    public function testAddABeerToABar()
    {
        $user = $this->createUser();
        $bar = $this->createBar();
        $beer = $this->createBeer();
        $client = static::createClient();

        $data = array(
            "token" => $this->forgeTokenFromUser($user),
            "id" => $beer->getId()
        );

        $client->request('POST', '/bars/add/'.$bar->getId()."/beer",
            $data
        );

        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $bar = $this->barRepository->findOneBy(array("id" => $bar->getId()));
        $this->assertCount(1, $bar->getBeers());
        $jsonBeer = $bar->getBeers()[0];
        $this->assertEquals($jsonBeer["id"], $beer->getId());
        $this->assertEquals($jsonBeer["name"], $beer->getName());
    }

    public function testEdit()
    {
        $user = $this->createUser();
        $bar = $this->createBar();
        $client = static::createClient();
        $data = array(
            "token" => $this->forgeTokenFromUser($user),
            "name" => "mitsia",
            "address" => "a_new_address",
            "latitude" => 48.866663,
            "longitude" => 2.333334,
            "happyHourStart" => 17,
            "happyHourEnd" => 20
        );
        $client->request('PUT', '/bars/edit/' . $bar->getId(),
            $data
        );
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $bar = $this->barRepository->findOneBy(array("name" => $data["name"]));
        $this->assertNotNull($bar);
        $this->assertEquals($bar->getName(), $data["name"]);
        $this->assertEquals($bar->getAddress(), $data["address"]);
        $this->assertEquals($bar->getLatitude(), $data["latitude"]);
        $this->assertEquals($bar->getLongitude(), $data["longitude"]);
        $this->assertEquals($bar->getHappyHourStart(), $data["happyHourStart"]);
        $this->assertEquals($bar->getHappyHourEnd(), $data["happyHourEnd"]);
    }

    public function testCannotEditOnUnknownId()
    {
        $user = $this->createUser();
        $client = static::createClient();
        $data = array(
            "token" => $this->forgeTokenFromUser($user),
            "name" => "mitsia",
            "address" => "a_new_address",
            "latitude" => 48.866663,
            "longitude" => 2.333334,
            "happyHourStart" => 17,
            "happyHourEnd" => 20
        );
        $client->request('PUT', '/bars/edit/1',
            $data
        );
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    public function testDelete()
    {
        $user = $this->createUser();
        $bar = $this->createBar();
        $client = static::createClient();

        $client->request('DELETE', '/bars/delete/' . $bar->getId(), array("token"=>$this->forgeTokenFromUser($user)));

        $this->assertEquals(204, $client->getResponse()->getStatusCode());

        $this->assertNull($this->barRepository->findOneById($bar->getId()));
    }

    public function testCannotDeleteOnUnknownId()
    {
        $user = $this->createUser();
        $client = static::createClient();

        $client->request('DELETE', '/bars/delete/1', array("token"=>$this->forgeTokenFromUser($user)));

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }
}
