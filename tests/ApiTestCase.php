<?php

namespace App\Tests;

use App\Entity\Bar;
use App\Entity\Beer;
use App\Entity\Brewery;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectRepository;

class ApiTestCase extends WebTestCase
{
    /**
     * @var ObjectRepository
     */
    protected $userRepository;

    /**
     * @var ObjectRepository
     */
    protected $barRepository;

    /**
     * @var ObjectRepository
     */
    protected $beerRepository;
    /**
     * @var ObjectRepository
     */
    protected $breweryRepository;
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @param $pseudo
     * @param $password
     * @return string
     */
    public static function forgeToken($pseudo, $password): string
    {
        return base64_encode($pseudo . ':' . $password);
    }

    /**
     * @param $pseudo
     * @param $password
     * @return string
     */
    public static function forgeTokenFromUser(User $user): string
    {
        return base64_encode($user->getPseudo() . ':' . $user->getPassword());
    }


    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $this->userRepository = $this->em->getRepository(User::class);
        $this->barRepository = $this->em->getRepository(Bar::class);
        $this->beerRepository = $this->em->getRepository(Beer::class);
        $this->breweryRepository = $this->em->getRepository(Brewery::class);
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();
        $entities = $this->userRepository->findAll();
        foreach ($entities as $entity) {
            $this->em->remove($entity);
        }

        $entities = $this->barRepository->findAll();
        foreach ($entities as $entity) {
            $this->em->remove($entity);
        }

        $entities = $this->beerRepository->findAll();
        foreach ($entities as $entity) {
            $this->em->remove($entity);
        }
        $entities = $this->breweryRepository->findAll();
        foreach ($entities as $entity) {
            $this->em->remove($entity);
        }
        $this->em->flush();
        $this->em->close();
        $this->em = null; // avoid memory leaks
    }

    public function createUser()
    {
        $user = new User();
        $user->setPseudo('mitsi');
        $user->setPassword('dev');
        $this->em->persist($user);
        $this->em->flush();
        return $user;
    }

    public function createBar()
    {
        $bar = new Bar();
        $bar->setName("a bar");
        $bar->setAddress("an address");
        $bar->setHappyHourStart("17");
        $bar->setHappyHourEnd("19");
        $bar->setLatitude(48.856614);
        $bar->setLongitude(2.352222);
        $this->em->persist($bar);
        $this->em->flush();
        return $bar;
    }

    public function associateBarWithBeer(Bar $bar, Beer $beer)
    {
        $bar->addBeer($beer);
        $beer->addBar($bar);

        $this->em->persist($bar);
        $this->em->persist($beer);
        $this->em->flush();
    }

    public function createBeer(){
        $beer = new Beer();
        $beer->setBrand("a brand");
        $beer->setColor("a color");
        $beer->setCountry("a country");
        $beer->setDegree(7.5);
        $beer->setName("a name");
        $beer->setPrice(2.30);
        $beer->setType("a type");
        $this->em->persist($beer);
        $this->em->flush();
        return $beer;
    }

    public function createBrewery(){
        $brewery = new Brewery();
        $brewery->setName("a_brewery");
        $brewery->setAddress("an_address");
        $brewery->setCreationDate(new \DateTime());
        $brewery->setCountry("a_country");
        $brewery->setBrand("a_brand");
        $brewery->setDescription("a_description");
        $this->em->persist($brewery);
        $this->em->flush();
        return $brewery;
    }

}