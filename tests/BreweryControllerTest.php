<?php
/**
 * Created by PhpStorm.
 * User: mitsi
 * Date: 18/01/2018
 * Time: 12:24
 */

namespace App\Controller;


use App\Entity\Brewery;
use App\Tests\ApiTestCase;

class BreweryControllerTest extends ApiTestCase
{

    public function testGetAllEmpty()
    {
        $client = static::createClient();
        $client->request('GET', '/breweries/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals([], json_decode($client->getResponse()->getContent(), true));
    }

    public function testGetAll()
    {
        $brewery = $this->createBrewery();
        $client = static::createClient();
        $client->request('GET', '/breweries/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(1, $content);
        $content = $content[0];


        $this->assertEquals($brewery->getName(), $content["name"]);
        $this->assertEquals($brewery->getAddress(), $content["address"]);
        $this->assertEquals($brewery->getCreationDate()->getTimestamp(), $content["creationDate"]["timestamp"]);
        $this->assertEquals($brewery->getCountry(), $content["country"]);
        $this->assertEquals($brewery->getBrand(), $content["brand"]);
        $this->assertEquals($brewery->getDescription(), $content["description"]);
    }

    public function testGet()
    {
        $brewery = $this->createBrewery();
        $client = static::createClient();
        $client->request('GET', '/breweries/' . $brewery->getId());

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);

        $this->assertEquals($brewery->getName(), $content["name"]);
        $this->assertEquals($brewery->getAddress(), $content["address"]);
        $this->assertEquals($brewery->getCreationDate()->getTimestamp(), $content["creationDate"]["timestamp"]);
        $this->assertEquals($brewery->getCountry(), $content["country"]);
        $this->assertEquals($brewery->getBrand(), $content["brand"]);
        $this->assertEquals($brewery->getDescription(), $content["description"]);
    }

    public function testAdd()
    {
        $client = static::createClient();
        $user = $this->createUser();
        $data = array(
            "token" => $this->forgeTokenFromUser($user),
            "name" => "a_name",
            "address" => "an_address",
            "creationDate" => new \DateTime(),
            "country" => "a_country",
            "brand" => "a_brand",
            "description" => "a_description"
        );

        $client->request('POST', '/breweries/add', $data);

        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $brewery = $this->breweryRepository->findOneByName($data["name"]);
        $this->assertEquals($brewery->getName(), $data["name"]);
        $this->assertEquals($brewery->getAddress(), $data["address"]);
        $this->assertEquals($brewery->getCreationDate()->getTimestamp(), $data["creationDate"]->getTimestamp());
        $this->assertEquals($brewery->getCountry(), $data["country"]);
        $this->assertEquals($brewery->getBrand(), $data["brand"]);
        $this->assertEquals($brewery->getDescription(), $data["description"]);
    }

    public function testEdit()
    {
        $client = static::createClient();
        $createdBrewery = $this->createBrewery();
        $user = $this->createUser();
        $data = array(
            "token" => $this->forgeTokenFromUser($user),
            "name" => "a_namae",
            "address" => "an_addressa",
            "creationDate" => new \DateTime(),
            "country" => "a_countrya",
            "brand" => "a_branda",
            "description" => "a_descriptiona"
        );

        $client->request('PUT', '/breweries/edit/' . $createdBrewery->getId(), $data);

        $this->assertEquals(204, $client->getResponse()->getStatusCode());

        $brewery = $this->em->getRepository(Brewery::class)->findOneByName($data["name"]);
        $this->assertEquals($brewery->getName(), $data["name"]);
        $this->assertEquals($brewery->getAddress(), $data["address"]);
        $this->assertEquals($brewery->getCreationDate()->getTimestamp(), $data["creationDate"]->getTimestamp());
        $this->assertEquals($brewery->getCountry(), $data["country"]);
        $this->assertEquals($brewery->getBrand(), $data["brand"]);
        $this->assertEquals($brewery->getDescription(), $data["description"]);
    }

    public function testDelete()
    {
        $user = $this->createUser();
        $brewery = $this->createBrewery();
        $client = static::createClient();

        $client->request('DELETE', '/breweries/delete/' . $brewery->getId(), array("token" => $this->forgeTokenFromUser($user)));

        $this->assertEquals(204, $client->getResponse()->getStatusCode());

        $this->assertNull($this->em->getRepository(Brewery::class)->findOneById($brewery->getId()));
    }
}
