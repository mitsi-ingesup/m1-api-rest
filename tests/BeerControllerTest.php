<?php
/**
 * Created by PhpStorm.
 * User: mitsi
 * Date: 18/01/2018
 * Time: 10:47
 */

namespace App\Controller;


use App\Tests\ApiTestCase;

class BeerControllerTest extends ApiTestCase
{


    // TODO : Ajouter les bars à une bière
    // TODO : Vérifier que les bars ne sont pas supprimées quand on supprime une bière

    public function testGetAll()
    {
        $client = static::createClient();
        $client->request('GET', '/beers/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals([], json_decode($client->getResponse()->getContent(), true));
    }

    public function testGet()
    {
        $client = static::createClient();
        $beer = $this->createBeer();
        $client->request('GET', '/beers/' . $beer->getId());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $json = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals($beer->getId(), $json["id"]);
        $this->assertEquals($beer->getName(), $json["name"]);
        $this->assertEquals($beer->getType(), $json["type"]);
        $this->assertEquals($beer->getColor(), $json["color"]);
        $this->assertEquals($beer->getDegree(), $json["degree"]);
        $this->assertEquals($beer->getPrice(), $json["price"]);
        $this->assertEquals($beer->getCountry(), $json["country"]);
        $this->assertEquals($beer->getBrand(), $json["brand"]);
    }

    public function testAdd()
    {
        $user = $this->createUser();
        $client = static::createClient();
        $data = array(
            "token" => $this->forgeTokenFromUser($user),
            "name" => "a_name",
            "type" => "a_type",
            "color" => "a_color",
            "degree" => 5.6,
            "price" => 2.30,
            "country" => "a_country",
            "brand" => "a_brand"
        );

        $client->request('POST', '/beers/add', $data);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertNotNull(json_decode($client->getResponse()->getContent(), true)["id"]);

        $beer = $this->beerRepository->findOneBy(array("name" => $data["name"]));

        $this->assertNotNull($beer);
        $this->assertEquals($beer->getName(), $data["name"]);
        $this->assertEquals($beer->getType(), $data["type"]);
        $this->assertEquals($beer->getColor(), $data["color"]);
        $this->assertEquals($beer->getDegree(), $data["degree"]);
        $this->assertEquals($beer->getPrice(), $data["price"]);
        $this->assertEquals($beer->getCountry(), $data["country"]);
        $this->assertEquals($beer->getBrand(), $data["brand"]);
    }

    public function testEdit()
    {
        $user = $this->createUser();
        $beer = $this->createBeer();
        $client = static::createClient();
        $data = array(
            "token" => $this->forgeTokenFromUser($user),
            "name" => "a_namea",
            "type" => "a_typea",
            "color" => "a_colora",
            "degree" => 5.67,
            "price" => 2.37,
            "country" => "a_countrya",
            "brand" => "a_branda"
        );

        $client->request('PUT', '/beers/edit/'.$beer->getId(), $data);
        $this->assertEquals(204, $client->getResponse()->getStatusCode());

        $beer = $this->beerRepository->findOneBy(array("name" => $data["name"]));

        $this->assertNotNull($beer);
        $this->assertEquals($beer->getName(), $data["name"]);
        $this->assertEquals($beer->getType(), $data["type"]);
        $this->assertEquals($beer->getColor(), $data["color"]);
        $this->assertEquals($beer->getDegree(), $data["degree"]);
        $this->assertEquals($beer->getPrice(), $data["price"]);
        $this->assertEquals($beer->getCountry(), $data["country"]);
        $this->assertEquals($beer->getBrand(), $data["brand"]);
    }

    public function testDelete()
    {
        $user = $this->createUser();
        $bar = $this->createBeer();
        $client = static::createClient();

        $client->request('DELETE', '/beers/delete/' . $bar->getId(), array("token"=>$this->forgeTokenFromUser($user)));

        $this->assertEquals(204, $client->getResponse()->getStatusCode());

        $this->assertNull($this->barRepository->findOneById($bar->getId()));
    }
}
