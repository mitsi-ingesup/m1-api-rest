<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180113205041 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE bar_beer (bar_id INT NOT NULL, beer_id INT NOT NULL, INDEX IDX_5F36729F89A253A (bar_id), INDEX IDX_5F36729FD0989053 (beer_id), PRIMARY KEY(bar_id, beer_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE beer_bar (beer_id INT NOT NULL, bar_id INT NOT NULL, INDEX IDX_EF997CE7D0989053 (beer_id), INDEX IDX_EF997CE789A253A (bar_id), PRIMARY KEY(beer_id, bar_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bar_beer ADD CONSTRAINT FK_5F36729F89A253A FOREIGN KEY (bar_id) REFERENCES bar (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE bar_beer ADD CONSTRAINT FK_5F36729FD0989053 FOREIGN KEY (beer_id) REFERENCES beer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE beer_bar ADD CONSTRAINT FK_EF997CE7D0989053 FOREIGN KEY (beer_id) REFERENCES beer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE beer_bar ADD CONSTRAINT FK_EF997CE789A253A FOREIGN KEY (bar_id) REFERENCES bar (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE bar_beer');
        $this->addSql('DROP TABLE beer_bar');
    }
}
