<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BarRepository")
 */
class Bar
{
    public function __construct()
    {
        $this->beers = [];
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $address;

    /**
     * @ORM\Column(type="float")
     */
    private $latitude;

    /**
     * @ORM\Column(type="float")
     */
    private $longitude;

    /**
     * @ORM\Column(type="integer")
     */
    private $happyHourStart;

    /**
     * @ORM\Column(type="integer")
     */
    private $happyHourEnd;

    /**
     * Many Bars have Many Beers.
     * @ORM\ManyToMany(targetEntity="Beer", inversedBy="bars")
     */
    private $beers;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getHappyHourStart()
    {
        return $this->happyHourStart;
    }

    /**
     * @param mixed $happyHourStart
     */
    public function setHappyHourStart($happyHourStart): void
    {
        $this->happyHourStart = $happyHourStart;
    }

    /**
     * @return mixed
     */
    public function getHappyHourEnd()
    {
        return $this->happyHourEnd;
    }

    /**
     * @param mixed $happyHourEnd
     */
    public function setHappyHourEnd($happyHourEnd): void
    {
        $this->happyHourEnd = $happyHourEnd;
    }

    /**
     * @return mixed
     */
    public function getBeers()
    {
        $output = [];
        foreach ($this->beers as $beer){
            $output[] = ["id"=>$beer->getId(), "name"=>$beer->getName()];
        }
        return $output;
    }

    /**
     * @param Beer $beer
     */
    public function addBeer($beer): void
    {
        $this->beers[] = $beer;
    }



}
