<?php
/**
 * Created by PhpStorm.
 * User: mitsi
 * Date: 13/01/2018
 * Time: 21:54
 */

namespace App\Controller;


use App\Entity\Bar;
use App\Entity\Beer;
use App\Entity\User;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BarController extends ApiController
{

    // TODO : Ajouter les bières à un bar
    // TODO : Vérifier que les bieres ne sont pas supprimées quand on supprime un bar

    public function getAll()
    {
        return new Response($this->serializeData($this->getDoctrine()->getRepository(Bar::class)->findAll()));
    }

    public function get($id)
    {
        return new Response($this->serializeData($this->getDoctrine()->getRepository(Bar::class)->findOneBy(array("id" => $id))));
    }

    public function getByTown($id)
    {
        throw new FatalErrorException("ControllerNotImplemented");
    }

    public function add(Request $request)
    {
        ApiController::getUserFromToken($request, $this->getDoctrine()->getRepository(User::class));
        $postParams = $request->request;
        $this->checkBarFiels($postParams);

        $bar = new Bar();
        $bar->setName($postParams->get("name"));
        $bar->setAddress($postParams->get("address"));
        $bar->setLatitude($postParams->get("latitude"));
        $bar->setLongitude($postParams->get("longitude"));
        $bar->setHappyHourStart($postParams->get("happyHourStart"));
        $bar->setHappyHourEnd($postParams->get("happyHourEnd"));
        $this->getDoctrine()->getManager()->persist($bar);
        $this->getDoctrine()->getManager()->flush();
        return new Response("", 204);
    }

    public function addABeer(Request $request, $id)
    {
        ApiController::getUserFromToken($request, $this->getDoctrine()->getRepository(User::class));
        $bar = $this->getDoctrine()->getRepository(Bar::class)->findOneById($id);
        if($bar === null){
            throw new NotFoundHttpException("Bar with id ".$id." not found");
        }
        if($request->request->get("id") === null || empty($request->request->get("id"))){
            throw new BadRequestHttpException("beer id missing");
        }
        $beer = $this->getDoctrine()->getRepository(Beer::class)->findOneById($request->request->get("id"));

        $bar->addBeer($beer);
        $beer->addBar($bar);

        $this->getDoctrine()->getManager()->persist($bar);
        $this->getDoctrine()->getManager()->persist($beer);
        $this->getDoctrine()->getManager()->flush();

        return new Response("", 204);
    }

    public function edit(Request $request, $id)
    {
        ApiController::getUserFromToken($request, $this->getDoctrine()->getRepository(User::class));
        $bar = $this->getDoctrine()->getRepository(Bar::class)->findOneById($id);
        if ($bar === null) {
            throw new NotFoundHttpException("Bar with id " . $id . " not found");
        }

        $postParams = $request->request;
        $this->checkBarFiels($postParams);

        $bar->setName($postParams->get("name"));
        $bar->setAddress($postParams->get("address"));
        $bar->setLatitude($postParams->get("latitude"));
        $bar->setLongitude($postParams->get("longitude"));
        $bar->setHappyHourStart($postParams->get("happyHourStart"));
        $bar->setHappyHourEnd($postParams->get("happyHourEnd"));
        $this->getDoctrine()->getManager()->persist($bar);
        $this->getDoctrine()->getManager()->flush();

        return new Response("", 204);
    }

    public function delete(Request $request, $id)
    {

        ApiController::getUserFromToken($request, $this->getDoctrine()->getRepository(User::class));
        $bar = $this->getDoctrine()->getRepository(Bar::class)->findOneById($id);
        if ($bar === null) {
            throw new NotFoundHttpException("Bar with id " . $id . " not found");
        }

        $this->getDoctrine()->getManager()->remove($bar);
        $this->getDoctrine()->getManager()->flush();

        return new Response("", 204);
    }

    /**
     * @param $postParams
     */
    public function checkBarFiels($postParams): void
    {
        if ($postParams->get("name") === null || empty($postParams->get("name"))) {
            throw new BadRequestHttpException("name not given");
        }

        if ($postParams->get("address") === null || empty($postParams->get("address"))) {
            throw new BadRequestHttpException("address not given");
        }

        if ($postParams->get("latitude") === null || empty($postParams->get("latitude"))) {
            throw new BadRequestHttpException("latitude not given");
        }

        if ($postParams->get("longitude") === null || empty($postParams->get("longitude"))) {
            throw new BadRequestHttpException("longitude not given");
        }

        if ($postParams->get("happyHourStart") === null || empty($postParams->get("happyHourStart"))) {
            throw new BadRequestHttpException("happyHourStart not given");
        }

        if ($postParams->get("happyHourEnd") === null || empty($postParams->get("happyHourEnd"))) {
            throw new BadRequestHttpException("happyHourEnd not given");
        }
    }
}