<?php

namespace App\Controller;

use App\Entity\Beer;
use App\Entity\User;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BeerController extends ApiController
{
    public function getAll()
    {
        return new Response($this->serializeData($this->getDoctrine()->getRepository(Beer::class)->findAll()));
    }

    public function get($id)
    {
        return new Response($this->serializeData($this->getDoctrine()->getRepository(Beer::class)->findOneById($id)));
    }

    public function getByTown($id)
    {
        throw new FatalErrorException("Not Implemented !");
    }

    public function add(Request $request)
    {
        ApiController::getUserFromToken($request, $this->getDoctrine()->getRepository(User::class));
        $postParams = $request->request;
        $this->checkBeerFiels($postParams);

        $beer = new Beer();

        $beer->setName($postParams->get("name"));
        $beer->setType($postParams->get("type"));
        $beer->setColor($postParams->get("color"));
        $beer->setDegree($postParams->get("degree"));
        $beer->setPrice($postParams->get("price"));
        $beer->setCountry($postParams->get("country"));
        $beer->setBrand($postParams->get("brand"));

        $this->getDoctrine()->getManager()->persist($beer);
        $this->getDoctrine()->getManager()->flush();
        return new Response(ApiController::serializeData(array("id" => $beer->getId())), 200);
    }

    public function edit(Request $request, $id)
    {
        ApiController::getUserFromToken($request, $this->getDoctrine()->getRepository(User::class));
        $beer = $this->getDoctrine()->getRepository(Beer::class)->findOneById($id);
        if ($beer === null) {
            throw new NotFoundHttpException("Beer with id " . $id . " not found");
        }
        $postParams = $request->request;
        $this->checkBeerFiels($postParams);
        $beer->setName($postParams->get("name"));
        $beer->setType($postParams->get("type"));
        $beer->setColor($postParams->get("color"));
        $beer->setDegree($postParams->get("degree"));
        $beer->setPrice($postParams->get("price"));
        $beer->setCountry($postParams->get("country"));
        $beer->setBrand($postParams->get("brand"));

        $this->getDoctrine()->getManager()->persist($beer);
        $this->getDoctrine()->getManager()->flush();
        return new Response("", 204);
    }

    public function delete(Request $request, $id)
    {
        ApiController::getUserFromToken($request, $this->getDoctrine()->getRepository(User::class));
        $beer = $this->getDoctrine()->getRepository(Beer::class)->findOneById($id);
        if ($beer === null) {
            throw new NotFoundHttpException("Beer with id " . $id . " not found");
        }

        $this->getDoctrine()->getManager()->remove($beer);
        $this->getDoctrine()->getManager()->flush();

        return new Response("", 204);
    }

    /**
     * @param $postParams
     */
    public function checkBeerFiels($postParams): void
    {
        if ($postParams->get("name") === null || empty($postParams->get("name"))) {
            throw new BadRequestHttpException("name not given");
        }

        if ($postParams->get("type") === null || empty($postParams->get("type"))) {
            throw new BadRequestHttpException("type not given");
        }

        if ($postParams->get("color") === null || empty($postParams->get("color"))) {
            throw new BadRequestHttpException("color not given");
        }

        if ($postParams->get("degree") === null || empty($postParams->get("degree"))) {
            throw new BadRequestHttpException("degree not given");
        }

        if ($postParams->get("price") === null || empty($postParams->get("price"))) {
            throw new BadRequestHttpException("price not given");
        }

        if ($postParams->get("country") === null || empty($postParams->get("country"))) {
            throw new BadRequestHttpException("country not given");
        }

        if ($postParams->get("brand") === null || empty($postParams->get("brand"))) {
            throw new BadRequestHttpException("brand not given");
        }
    }
}