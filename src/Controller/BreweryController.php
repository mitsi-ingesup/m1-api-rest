<?php
/**
 * Created by PhpStorm.
 * User: mitsi
 * Date: 13/01/2018
 * Time: 21:54
 */

namespace App\Controller;

use App\Entity\Brewery;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\FatalErrorException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BreweryController extends ApiController
{
    public function getAll()
    {
        return new Response($this->serializeData($this->getDoctrine()->getRepository(Brewery::class)->findAll()));
    }

    public function get($id)
    {
        return new Response($this->serializeData($this->getDoctrine()->getRepository(Brewery::class)->findOneById($id)));
    }

    public function getByTown($id)
    {
        throw new FatalErrorException("ControllerNotImplemented");
    }

    public function add(Request $request)
    {
        ApiController::getUserFromToken($request, $this->getDoctrine()->getRepository(User::class));
        $postParams = $request->request;
        $this->checkBreweryFiels($postParams);

        $brewery = new Brewery();
        $brewery->setName($postParams->get("name"));
        $brewery->setAddress($postParams->get("address"));
        $brewery->setCreationDate($postParams->get("creationDate"));
        $brewery->setCountry($postParams->get("country"));
        $brewery->setBrand($postParams->get("brand"));
        $brewery->setDescription($postParams->get("description"));
        $this->getDoctrine()->getManager()->persist($brewery);
        $this->getDoctrine()->getManager()->flush();
        return new Response("", 204);
    }

    public function edit(Request $request, $id)
    {
        ApiController::getUserFromToken($request, $this->getDoctrine()->getRepository(User::class));
        $brewery = $this->getDoctrine()->getRepository(Brewery::class)->findOneById($id);
        if ($brewery === null){
            throw new NotFoundHttpException("Brewery with id ".$id." not found");
        }
        $postParams = $request->request;
        $this->checkBreweryFiels($postParams);

        $brewery->setName($postParams->get("name"));
        $brewery->setAddress($postParams->get("address"));
        $brewery->setCreationDate($postParams->get("creationDate"));
        $brewery->setCountry($postParams->get("country"));
        $brewery->setBrand($postParams->get("brand"));
        $brewery->setDescription($postParams->get("description"));

        $this->getDoctrine()->getManager()->persist($brewery);
        $this->getDoctrine()->getManager()->flush();

        return new Response("", 204);
    }

    public function delete(Request $request, $id)
    {
        ApiController::getUserFromToken($request, $this->getDoctrine()->getRepository(User::class));
        $brewery = $this->getDoctrine()->getRepository(Brewery::class)->findOneById($id);
        if ($brewery === null) {
            throw new NotFoundHttpException("Brewery with id " . $id . " not found");
        }

        $this->getDoctrine()->getManager()->remove($brewery);
        $this->getDoctrine()->getManager()->flush();

        return new Response("", 204);
    }

    private function checkBreweryFiels($postParams): void
    {
        if ($postParams->get("name") === null || empty($postParams->get("name"))) {
            throw new BadRequestHttpException("name not given");
        }

        if ($postParams->get("address") === null || empty($postParams->get("address"))) {
            throw new BadRequestHttpException("address not given");
        }

        if ($postParams->get("creationDate") === null || empty($postParams->get("creationDate"))) {
            throw new BadRequestHttpException("creationDate not given");
        }

        if ($postParams->get("country") === null || empty($postParams->get("country"))) {
            throw new BadRequestHttpException("country not given");
        }

        if ($postParams->get("brand") === null || empty($postParams->get("brand"))) {
            throw new BadRequestHttpException("brand not given");
        }

        if ($postParams->get("description") === null || empty($postParams->get("description"))) {
            throw new BadRequestHttpException("description not given");
        }

    }
}