<?php

namespace App\Controller;


use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class UserController extends ApiController
{
    public function getAll()
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        foreach ($users as $user) {
            $user->hidePassword();
        }
        return new Response(ApiController::serializeData($users));
    }

    public function get($id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(array("id" => $id));
        if ($user === null) {
            throw new NotFoundHttpException();
        }
        $user->hidePassword();
        return new Response(ApiController::serializeData($user));
    }

    public function add(Request $request)
    {
        $pseudo = $request->request->get("pseudo");
        if (!isset($pseudo) || empty($pseudo)) {
            throw new BadRequestHttpException("Pseudo not given");
        }

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(array("pseudo" => $pseudo));

        if ($user !== null) {
            throw new BadRequestHttpException("Pseudo already existing");
        }

        $password = $request->request->get("password");

        if (!isset($password) || empty($password)) {
            throw new BadRequestHttpException("Password not given");
        }

        $user = new User();
        $user->setPseudo($pseudo);
        $user->setPassword($password);
        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();
        return new Response("", 204);
    }

    public function edit(Request $request)
    {
        $user = ApiController::getUserFromToken($request, $this->getDoctrine()->getRepository(User::class));
        $newPassword = $request->request->get("newPassword");
        if (!isset($newPassword) || empty($newPassword)) {
            throw new BadRequestHttpException("Password not given");
        }

        $user->setPassword($newPassword);
        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();
        return new Response("", 204);
    }

    public function delete(Request $request)
    {
        $user = ApiController::getUserFromToken($request, $this->getDoctrine()->getRepository(User::class));
        $this->getDoctrine()->getManager()->remove($user);
        $this->getDoctrine()->getManager()->flush();
        return new Response("", 204);
    }
}