<?php
/**
 * Created by PhpStorm.
 * User: mitsi
 * Date: 18/01/2018
 * Time: 10:12
 */

namespace App\Controller;


use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ApiController extends Controller
{

    /**
     * @param Request $request
     * @param ObjectRepository $repository
     * @return Object
     */
    public static function getUserFromToken(Request $request, ObjectRepository $repository): Object
    {
        $tokenParam = $request->request->get("token");
        if (!isset($tokenParam) || empty($tokenParam)) {
            throw new BadRequestHttpException("Token not given");
        }
        $token = base64_decode($tokenParam);
        if (empty($token)) {
            throw new BadRequestHttpException("Token malformed");
        }
        $parts = explode(":", $token);
        if (count($parts) != 2) {
            throw new BadRequestHttpException("Token malformed");
        }
        $pseudo = $parts[0];
        $password = $parts[1];

        if (!isset($pseudo) || empty($pseudo)) {
            throw new BadRequestHttpException("Pseudo not given");
        }
        if (!isset($password) || empty($password)) {
            throw new BadRequestHttpException("Password not given");
        }

        $user = $repository->findOneBy(array("pseudo" => $pseudo, "password" => $password));

        if ($user === null) {
            throw new UnauthorizedHttpException("You need to be identified in your account");
        }

        return $user;
    }

    public static function serializeData($data)
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $serializer = new Serializer(array($normalizer), array($encoder));
        return $serializer->serialize($data, 'json');
    }
}